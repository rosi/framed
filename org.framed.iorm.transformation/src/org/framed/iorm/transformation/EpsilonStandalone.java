package org.framed.iorm.transformation;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.epsilon.common.parse.problem.ParseProblem;
import org.eclipse.epsilon.eol.IEolModule;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.models.IModel;

public abstract class EpsilonStandalone {

	protected IEolModule module;

	protected Object result;

	public abstract IEolModule createModule();

	public abstract String getSource();

	public abstract List<IModel> getModels() throws Exception;
  
	public void postProcess() {};

	public void preProcess() {};
  
	public void execute() throws Exception {
	  module = createModule();
		try {
			module.parse(getTransformationFile());
		} catch (Exception e) { e.printStackTrace(); }
		if (module.getParseProblems().size() > 0) {
			System.err.println("Parse errors occured...");
			for(ParseProblem problem : module.getParseProblems())
				System.err.println(problem.toString());
			System.exit(-1);
		}
	for (IModel model : getModels()) {
      module.getContext().getModelRepository().addModel(model);
    }
    preProcess();
    result = execute(module);
    postProcess();
    module.getContext().getModelRepository().dispose();
  }
  
  protected Object execute(IEolModule module) throws EolRuntimeException {
    return module.execute();
  }

  protected URI getTransformationFile() {
    URL fileURL = null;
	try {
		fileURL = new File(getSource()).toURI().toURL();
	} catch (MalformedURLException e1) {
		e1.printStackTrace();
	}
	

    try {
  	  System.out.println("Output file is " + FileLocator.toFileURL(fileURL).toString());

    	return FileLocator.toFileURL(fileURL).toURI();
    } catch (URISyntaxException e) { e.printStackTrace(); } catch (IOException e) {
		e.printStackTrace();
	}
    return null;
  }
}
